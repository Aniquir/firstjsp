<%@ page import="java.util.Date" %>
<%@ page import="sun.util.resources.CalendarData" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %>
<%!
    Random random = new Random();
    int sum = 0;
%>
<html>
<body>
<h1>To jest przykład strony JSP</h1>

Dzisiaj jest:
<%=
new Date()
%>
<br>
Wpisany adres:
<%=
request.getRequestURI()
%>
<br>
Liczby losowe:

<%
    for (int i = 0; i < 10; i++) {
        int x = random.nextInt(1000);
        out.print(" " + x);
        sum += x;
    }
%>
<br>
Suma liczb:
<%
out.print(sum);
%>
</body>
</html>
